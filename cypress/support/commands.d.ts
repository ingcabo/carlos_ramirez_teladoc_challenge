// in cypress/support/index.d.ts
// load type definitions that come with Cypress module
/// <reference types="cypress" />

declare namespace Cypress {
  
    interface Chainable {
      /**
       * Click on Selector Name
       *
       * @example
       * cy
       *   .clickOnButton()
       */
      clickOnButton()

    }

    interface Chainable {
      /**
       * fill Input Text
       *
       * @example
       * cy
       *   .fillInputText()
       */
       fillInputText()

    }


    interface Chainable {
      /**
       * validate be visible
       *
       * @example
       * cy
       *   .selectorBeVisible()
       */
       selectorBeVisible()

    }

    interface Chainable {
      /**
       * 
       *
       * @example
       * cy
       *   .fillInputTextReplaceSelectorKey()
       */
       fillInputTextReplaceSelectorKey()

    }    


    interface Chainable {
      /**
       * 
       *
       * @example
       * cy
       *   .selectOptionContaining()
       */
       selectOptionContaining()

    } 
    
    
  }
  
  
  
