import { Given, When, Then } from 'cypress-cucumber-preprocessor/steps'
import webTablesPage from './webTablesPage'




Given('I visit webTablesPage', () => {
  webTablesPage.visic()
})

Given('I click {string} button', selectorName => {

  webTablesPage.buttonClick(selectorName)
})

When('I should see labels and titles', () => {
  webTablesPage.validateAddUserForm()
})

When('I fill First Name with {string}', username => {
  webTablesPage.fillFirstName(username)
})

When('I fill Last Name with {string}', lastName => {
  webTablesPage.fillLastName(lastName)
})

When('I fill User Name with {string}', usaerName => {
  webTablesPage.fillUserName(usaerName)
})

When('I fill Password with {string}', password => {
  webTablesPage.fillPassword(password)
})

When('I click on Customer {string}', customType => {
  webTablesPage.selectCustomerType(customType)
})


When('I select Role {string}', roleDesc => {
  webTablesPage.selectRoleId(roleDesc)
})

When('I fill E mail with {string}', email => {
  webTablesPage.fillEmail(email)
})

When('I fill Cell Phone with {string}', phone => {
  webTablesPage.fillPhone(phone)
})

When('I click on {string} button', selectorName => {
  webTablesPage.buttonClick(selectorName)
})

Then('I should see webtables with new user using email {string} and name {string}', (email,name) => {
  webTablesPage.validateTableInfo(email,name)
})

When('I fill form data', (table) => {
    table.hashes().forEach((row) => {
      webTablesPage.fillFirstName(row.FirstName)
      webTablesPage.fillLastName(row.LastName)
      webTablesPage.fillUserName(row.UserName)
      webTablesPage.fillPassword(row.Password)
      webTablesPage.selectCustomerType(row.Customer)
      webTablesPage.selectRoleId(row.Role)
      webTablesPage.fillEmail(row.Email)
      webTablesPage.fillPhone(row.Phone)
    })


    Then('I should see webtables with new user', (table) => {
      webTablesPage.validateTableInfoIntegrity(table)
    })  

})



When('I click on remove button for a user', async () =>  {

 await webTablesPage.selectUserToDeleteAndDelete()

})

When('I should see a message {string}', (text) =>  {

  webTablesPage.deleteMessageValidation(text)

})

Then('I should not see the user', () => {
  webTablesPage.userDeleteValidation()
})


