
const URL = 'angularjs-protractor/webtables/'
const ADD_USER_FORM_TITLE_SELECTOR ="h3"
const ADD_USER_FORM_TITLE_NAME="Add User"
const FORM_LABLE_SELECTOR= "td";
const FORM_FIRST_NAME_LABEL="First Name";
const FORM_LAST_NAME_LABEL="Last Name";
const FORM_USER_NAME_LABEL="User Name";
const FORM_PASSWORD_LABEL="Password";
const FORM_CUSTUMER_LABEL="Customer";
const FORM_ROLE_LABEL="Role";
const FORM_EMAIL_LABEL="E-mail";
const FORM_CELL_PHONE_LABEL="Cell Phone";
const FORM_SAVE_BUTTON_NAME="Save";
const FORM_CLOSE_BUTTON_NAME="Close";
const FORM_RADIO =".radio.ng-scope.ng-binding"; 
const FORM_CUSTUMMER_TYPE_AAA="Company AAA";
const FORM_CUSTUMMER_TYPE_BBB="Company BBB";
const FORM_FIRST_NAME_REQUIRE_SELECTOR="span[ng-show|='smartTableValidForm.FirstName.$error.required']";
const FORM_ROLE_ID_REQUIRE_SELECTOR="span[ng-show|='smartTableValidForm.RoleId.$error.required']";
const FORM_REQUIRED_VALUE="Required!";


const FORM_INPUT="input[name|='%%']";
const FORM_ROLE_ID_SELECTOR="select[name|='RoleId']";
const FORM_EMAIL_INPUT="input[name|='Email']";
const FORM_PHONE_INPUT="input[name|='Mobilephone']";

const REMOVE_BUTTON="i.icon.icon-remove"




const TIME = 7000;

class webTablesPage {

 // originalLength;
//  emailBefore;
constructor() {
  this.emailDelete;
}

  static visic() {
    cy.visit(URL,{timeout:TIME})
  }

  static buttonClick(selectorName) {
    cy.clickOnButton(selectorName,{timeout:TIME});
  }

  static validateAddUserForm() {

    cy.selectorBeVisible(FORM_FIRST_NAME_REQUIRE_SELECTOR,FORM_REQUIRED_VALUE);
    cy.selectorBeVisible(FORM_ROLE_ID_REQUIRE_SELECTOR,FORM_REQUIRED_VALUE);
    cy.selectorBeVisible(ADD_USER_FORM_TITLE_SELECTOR,ADD_USER_FORM_TITLE_NAME);
    cy.selectorBeVisible(FORM_LABLE_SELECTOR,FORM_FIRST_NAME_LABEL);
    cy.selectorBeVisible(FORM_LABLE_SELECTOR,FORM_LAST_NAME_LABEL);
    cy.selectorBeVisible(FORM_LABLE_SELECTOR,FORM_USER_NAME_LABEL);
    cy.selectorBeVisible(FORM_LABLE_SELECTOR,FORM_PASSWORD_LABEL);
    cy.selectorBeVisible(FORM_LABLE_SELECTOR,FORM_CUSTUMER_LABEL);
    cy.selectorBeVisible(FORM_LABLE_SELECTOR,FORM_ROLE_LABEL);
    cy.selectorBeVisible(FORM_LABLE_SELECTOR,FORM_EMAIL_LABEL);
    cy.selectorBeVisible(FORM_LABLE_SELECTOR,FORM_CELL_PHONE_LABEL);
    cy.selectorBeVisible(FORM_RADIO,FORM_CUSTUMMER_TYPE_AAA);
    cy.selectorBeVisible(FORM_RADIO,FORM_CUSTUMMER_TYPE_BBB);
    this.validateButton();

  }

  static validateButton() {
    cy.contains(FORM_SAVE_BUTTON_NAME).should('be.visible').and('be.disabled');
    cy.contains(FORM_CLOSE_BUTTON_NAME).should('be.visible').and('not.be.disabled');
  }

  static fillFirstName(textValue) {
   cy.fillInputTextReplaceSelectorKey(FORM_INPUT,FORM_FIRST_NAME_LABEL.replace(' ',''),textValue)
  }

  static fillLastName(textValue) {
    cy.fillInputTextReplaceSelectorKey(FORM_INPUT,FORM_LAST_NAME_LABEL.replace(' ',''),textValue);
  }

  static fillUserName(textValue) {
    cy.fillInputTextReplaceSelectorKey(FORM_INPUT,FORM_USER_NAME_LABEL.replace(' ',''),textValue);
  }


  static fillPassword(textValue) {
    cy.fillInputTextReplaceSelectorKey(FORM_INPUT,FORM_PASSWORD_LABEL.replace(' ',''),textValue);
  }


  static selectCustomerType(textValue) {
    cy.get(FORM_RADIO).contains(textValue).click();
  }


  static selectRoleId(textValue) {
    cy.selectOptionContaining(FORM_ROLE_ID_SELECTOR,textValue)
  }

  static fillEmail(textValue) {
    cy.fillInputText(FORM_EMAIL_INPUT,textValue);
  }

  static fillPhone(textValue) {
    cy.fillInputText(FORM_PHONE_INPUT,textValue);
  }

  static validateTableInfo(paramEmail, findBy){
  
  
    cy.get('tbody >tr td:nth-child(1)').each(($el, index, $list) =>{
      var text=$el.text()
      if(text.includes(findBy)){

          cy.get('tbody >tr td:nth-child(7)').eq(index).then(function(email){
              var emailTest=email.text()
              expect(emailTest.trim()).to.equal(paramEmail)

          })
      }

     })
  
  }

 
  static validateTableInfoIntegrity(table){

    cy.get('tbody >tr td:nth-child(7)').each(($el, index, $list) =>{
      var text=$el.text()

      table.hashes().forEach((row) => { 

        if(text.includes(row.Email)){

          cy.get('tbody >tr td:nth-child(1)').eq(index).then(function(firstName){
            var name=firstName.text()
            expect(name.trim()).to.equal(row.FirstName)
          })


          cy.get('tbody >tr td:nth-child(2)').eq(index).then(function(lastName){
            var name=lastName.text()
            expect(name.trim()).to.equal(row.LastName)
          })


          cy.get('tbody >tr td:nth-child(3)').eq(index).then(function(userName){
            var name=userName.text()
            expect(name.trim()).to.equal(row.UserName)
          })

        
          cy.get('tbody >tr td:nth-child(6)').eq(index).then(function(role){
            var name=role.text()
            expect(name.trim()).to.equal(row.Role)
          })

          cy.get('tbody >tr td:nth-child(7)').eq(index).then(function(email){
            var name=email.text()
            expect(name.trim()).to.equal(row.Email)
          })

          cy.get('tbody >tr td:nth-child(8)').eq(index).then(function(phone){
            var name=phone.text()
            expect(name.trim()).to.equal(row.Phone)
          })


          cy.get('tbody >tr td:nth-child(5)').eq(index).then(function(customer){
            var name=customer.text()
            expect(name.trim()).to.equal(row.Customer)
          })
          
        }
      })

    })

  }



  static async selectUserToDeleteAndDelete (){
   
   cy.get('tbody').find("tr").its('length').then((len) => {
    var index = len-1;
      cy.get('tbody >tr td:nth-child(7)').eq(index).then(function(email){
        var emailTo=email.text()
        this.emailDelete = emailTo
        cy.get('button').invoke('text').as('text')
        cy.wrap(emailTo).as('originalEmail');
        cy.get(REMOVE_BUTTON).eq(index).click({force:true})   
      })
     })
  }

  static deleteMessageValidation(text){
    cy.selectorBeVisible("p",text);
  }

  static userDeleteValidation(){


    cy.get('tbody').contains('td', this.originalEmailb).should('not.be.visible')
  }


}

export default webTablesPage
