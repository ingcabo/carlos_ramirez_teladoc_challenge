Feature: Add a user and validate user has been added

    As complete all fields add user form  
    I want to add a user
    As complete all fields add user form  using a table data 
    I want to add a user
    User deletion
    I want to delete a user

    Background:
        Given I visit webTablesPage

    
    Scenario: add a user complete all field of add user form 
        Given I click "Add User" button
        When I should see labels and titles
        And I fill First Name with "Carlos"
        And I fill Last Name with "Ramirez"
        And I fill User Name with "User Name"
        And I fill Password with "Password"
        And I click on Customer "Company BBB"
        And I select Role "Customer"
        And I fill E mail with "myemail@gmail.com"
        And I fill Cell Phone with "555-5555-5555"
        And I click on "Save" button
        Then I should see webtables with new user using email "myemail@gmail.com" and name "Carlos"

    Scenario: add a user complete all field of add user form but using a table
        Given I click "Add User" button
        When I should see labels and titles
        And I fill form data
        | FirstName | LastName | UserName | Password  | Customer    | Role        |  Email                  | Phone        |   
        | Carlos    | Ramirez  | cramirez | abc123    | Company BBB | Customer    |    myEmail@gmail.com    |  555-555--55 |
        And I click on "Save" button
        Then I should see webtables with new user
        | FirstName | LastName | UserName | Password  | Customer    | Role        |  Email                  | Phone        |   
        | Carlos    | Ramirez  | cramirez | abc123    | Company BBB | Customer    |    myEmail@gmail.com    |  555-555--55 |


    Scenario: delete a user
        Given I click on remove button for a user
        When I should see a message "Do you really want to delete the user?"
        And I click on "OK" button
        Then I should not see the user


        
